#!/bin/bash


if [ "$1" == "a" ] # Actions Server
then
	kill -9 $(ps -ef | grep "rasa run actions" | awk 'FNR==1 {print $2}')
elif [ "$1" == "s" ] # RASA Server
then
	kill -9 $(ps -ef | grep "rasa run -v" | awk 'FNR==1 {print $2}')
elif [ "$1" == "" ] # Both Servers
then
	kill -9 $(ps -ef | grep "rasa run actions" | awk 'FNR==1 {print $2}')
	kill -9 $(ps -ef | grep "rasa run -v" | awk 'FNR==1 {print $2}')
fi


sleep 1
ps -ef | grep "rasa run"

