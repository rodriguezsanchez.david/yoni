#RASA import
from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa.core.events import SlotSet
from rasa_sdk.forms import FormAction#, REQUESTED_SLOT
#from rasa.core.actions import Action
#Machine Learning
import spacy 
#APIs import
import facebook
#Custom Code
#import facebook_category_map as fcmap
import math


FACEBOOK_TOKEN='EAAhGNj1HzjsBADZCq6ZCwkxR4Gnvx7JdaGnWlaWp0vHdH0ZCXdfEeGQmua4CNzD9DX37ikfHu92o9XNL5YqsAzscEaPmB7rDllRgIJA6jiCKfeuZBI5XKReEWt99BqKdxLZCoi47LQqExyaAOafsuZBStwTZAJ5vraa2VLp8I18gQZDZD'
COUNTRY="Spain"# Restringimos las búequedas a lugares de España

class EstablecerNombre(Action):
    __nlp = None
    
    def __init__(self):
         self.__nlp = spacy.load('es_core_news_sm') 

    def name(self) -> Text:
        return "accion_establecer_nombre"
    
    def run(self, dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        eventList = []
        
        if tracker.get_slot('nombre') :
            dispatcher.utter_message('Hola ' + str(tracker.get_slot('nombre')).capitalize())
        else:
            doc = self.__nlp(tracker.latest_message['text'])
            for ent in doc.ents:
                if ent.label_ == 'PER':
                    dispatcher.utter_message('Hola '+ str(ent.text).capitalize() )
                    print(ent.text, ent.label_) 
                    eventList.append(SlotSet("nombre", str(ent.text).capitalize()))
                    print(e)
                else:
                    dispatcher.utter_message('Hola')
            
        return eventList

class BusquedaForm(FormAction):
    __graph = None
    __nlp = None
    
    def __init__(self):
        self.__graph = facebook.GraphAPI(access_token=FACEBOOK_TOKEN)
        self.__nlp = spacy.load('es_core_news_sm') 

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "busqueda_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""

        return ["servicio", "lugar"]

    def slot_mappings(self):
        # type: () -> Dict[Text: Union[Dict, List[Dict]]]
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {"servicio": self.from_entity(entity="servicio",
                                            intent=["busqueda_global",
                                                        "informar"]),
                "lugar": [self.from_entity(entity="lugar",
                                                intent=["busqueda_global",
                                                        "informar"]),
                               self.from_entity(entity="LOC")]
                }


    def submit(self,
               dispatcher: CollectingDispatcher,
               tracker: Tracker,
               domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""
            
        dispatcher.utter_template('utter_pcoord_busqueda', tracker)
        
        is_result  = False
        lugar_entity = ""
        location = None       
        places = None
        eventList = []
        result = []
                

        #Lugar logic        
        doc = self.__nlp(tracker.latest_message['text'])
        for ent in doc.ents:
            if ent.label_ == 'LOC':
              if not lugar_entity:
                  lugar_entity = ent.text  
              else:
                  lugar_entity += ' '+ent.text                 
              print(lugar_entity , ' -> ', ent.label_)     
        if lugar_entity:
            eventList.append(SlotSet("lugar", lugar_entity))
        print('busqueda_form::Spacy entities found: %s', doc.ents)     
        
        #si no encontramos el lugar en SpaCy, pero sí en nuestro modelo, lo tomamos
        if not lugar_entity:            
            lugar_entity = tracker.get_slot('lugar')
            print('busqueda_form::Lugar desde nuestro modelo custom: %s', lugar_entity)     
        if not lugar_entity:
            #print('busqueda_form::No se detectó el lugar')     
            print('WARNING -> busqueda_form::No se detectó el lugar')
        
        #Location Logic        
        if lugar_entity:   
            try:
                locations=self.__graph.search(type='place',
                          q=lugar_entity,
                          fields='location',
                          limit=50
                          )
                #print('locations = '+ str(locations))
                if len(locations['data']) > 1:
                    for loc in locations['data']:
                        #print(loc)
                        if 'country' in loc['location']:
                            if loc['location']['country'] == COUNTRY:                        
                                location = loc
                                break
                    if not location:
                        location = locations['data'][0]
                elif len(locations['data']) == 1:
                    location = loc
                print('busqueda_form::location = %s '+ str(location))
            except:
                location = None
                print('ERROR -> busqueda_form::Algo falló obteniendo las coordenadas de las ciudades en Facebook')
                dispatcher.utter_message("Lo siento pero el servicio de Facebook no está disponible. Intentalo más tarde.");
                
        if location:            
            lat = location['location']['latitude']
            lon = location['location']['longitude']            
            center_latlon = str(lat) + str(', ') + str(lon)
            print('busqueda_form::Latitude de %s %s', tracker.get_slot('lugar'), lat)
            print('busqueda_form::Latitude de %s %s', tracker.get_slot('lugar'), lon)
            try:
                places =self.__graph.search(type='place',
                          q=tracker.get_slot('servicio'),
                          center=center_latlon,
#                          center=tracker.get_slot('latlon'),
                          distance=5000,
                          fields='id,name,description,about,cover,checkins,location,overall_rating,overall_star_rating,link',
                          limit=20
                          )
            except:
                places = None
                print('ERROR -> busqueda_form::Algo falló obteniendo los lugares en Facebook')
                dispatcher.utter_message("Lo siento pero el servicio de Facebook no está disponible. Intentalo más tarde.");
        
        if places and len(places['data']) > 0:
            print('busqueda_form::places = %s', places)
            is_result = True
            
        if is_result :   
            result = places
        
        eventList.append(SlotSet("result", result))
        
        return eventList


class InfoDetalleForm(FormAction):
    __graph = None
#    __nlp = None
    
    def __init__(self):
        self.__graph = facebook.GraphAPI(access_token=FACEBOOK_TOKEN)
        #self.__nlp = spacy.load('es_core_news_sm') 

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "infodetalle_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""

        return ["seleccion", "result"]

    def slot_mappings(self):
        # type: () -> Dict[Text: Union[Dict, List[Dict]]]
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {"seleccion": self.from_entity(entity="seleccion",
                                            intent=["info_detalle"]),
                "result": [self.from_entity(entity="result",
                                                intent=["busqueda_global",
                                                        "informar"]),
                               self.from_entity(entity="LOC")]
                }


    def submit(self,
               dispatcher: CollectingDispatcher,
               tracker: Tracker,
               domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""
            
        dispatcher.utter_template('utter_pcoord_busqueda', tracker)

        return []


class MostrarResultados(Action):


    def name(self) -> Text:
        return "accion_mostrar_resultado"

    def run(self, dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        resultText = []
        placesText = ""
        num_items = 3

        result = tracker.get_slot('result')
        print('accion_mostrar_resultado::result: ' + str(result))
        channel = tracker.get_latest_input_channel()
        #print('channel: ', channel)

        if result:
            dispatcher.utter_template('utter_resultados', tracker)

            try:
                result['data'].sort(key=lambda a: a['checkins'], reverse = True)
                
                for r in result['data'][0:num_items]:
                    if placesText:
                        placesText = placesText + "\n"
                    placesText = placesText + "* " + str(r['name'])
                    if 'single_line_address' in r:
                        placesText = placesText + " en "+ str(r['single_line_address'])
                    else:
                        if 'city' in r['location']:
                            placesText = placesText + " en "+ str(r['location']['city'])
                        if 'street' in r['location']:
                            placesText = placesText + ", "+ str(r['location']['street'])
                #print(placesText)
            except:
                print('ERROR -> accion_mostrar_resultado::Algo falló ordenando los resultados de Facebook')
                dispatcher.utter_message('Lo siento, pero en estos momentos no puedo obtener la información solicitada', tracker)


            if channel == "facebook":
                elements = []
                for r in result['data'][0:num_items]:
                    #dispatcher.utter_message("test6");
                    item = dict()
                    item['title'] = r['name']
                    item['subtitle'] = "(Sin dirección)"
                    if 'single_line_address' in r:
                        item['subtitle'] = r['single_line_address']
                    else:
                        if 'street' in r['location']:
                            item['subtitle'] = r['location']['street']
                    if 'cover' in r:
                        item['image_url'] = r['cover']['source']
                    #item["buttons"] = []
                    elements.append(item)
              
                json_message = {  "attachment": 
                                   { "type": "template",
                                     "payload": 
                                         { "template_type": "list",
                                           "top_element_style": "compact",
                                           "elements": elements
                                           #"buttons": []
                                         }
                                   }
                               }

                #print('json_message :'+ str(json_message))
                dispatcher.utter_custom_json(json_message)
            elif channel == "slack":
                blocks = []
                #dispatcher.utter_message(placesText)
                try:
                    for r in result['data'][0:num_items]:
                        item = dict()
                        item['type'] = "section"
                        item['text'] = {"type": "mrkdwn", 
                                        'text': "*" + str(r['name']) + "*\n"}
                        if 'overall_rating' in r and r['overall_rating'] > 0:
                            for s in range(math.trunc(r['overall_rating'])):
                                item['text']['text'] = item['text']['text'] + ":star:"
                        elif 'overall_star_rating' in r:
                            for s in range(math.trunc(r['overall_star_rating'])):
                                item['text']['text'] = item['text']['text'] + ":star:"
                        if 'checkins' in r:                        
                            item['text']['text'] = item['text']['text'] + " " + str(r['checkins']) + " Visitantes"
                        if 'description' in r:
                            if len(r['description']) > 100:
                                #desc = str(r['description']).split(" ", math.trunc(len(r['description'])/100) )[1]
                                #item['text']['text'] = item['text']['text'] + "\n" + str(desc) + " ..."
                                item['text']['text'] = item['text']['text'] + "\n" + str(r['description'])[0:100] + " ..."
                            else:
                                item['text']['text'] = item['text']['text'] + "\n" + str(r['description'])
                        elif 'about' in r:
                            if len(r['about']) > 100:
                                #desc = str(r['about']).split(" ", math.trunc(len(r['about'])/100) )[1]
                                #item['text']['text'] = item['text']['text'] + "\n" + str(desc) + " ..."
                                item['text']['text'] = item['text']['text'] + "\n" + str(r['about'])[0:100] + " ..."
                            else:
                                item['text']['text'] = item['text']['text'] + "\n" + str(r['about'])
                        if 'street' in r['location']:
                            item['text']['text'] = item['text']['text'] + "\n_" + r['location']['city'] +", "+r['location']['street'] + "_"
                        if 'cover' in r:
                            item['accessory'] = {
                                                'type': 'image',
                                                'image_url':  r['cover']['source'],
                                                'alt_text': 'Image'
                                                }
                        blocks.append(item)
                    json_message = {'blocks':  blocks }
                    print('json_message :'+ str(json_message))
                    dispatcher.utter_custom_json(json_message)
                except:
                    dispatcher.utter_message("Lo siento, algún error ocurrió con los datos obtenidos de Facebook")
            else:
                dispatcher.utter_message(placesText)
        else:
            dispatcher.utter_template('utter_sin_resultados', tracker)            


        return []


class ActionDefaultAskAffirmation(Action):

   """Asks for an affirmation of the intent if NLU threshold is not met."""

   def name(self):

       return "action_default_ask_affirmation"


   def __init__(self):

       self.intent_mappings = {}

       # set the mapping
       self.intent_mappings['busqueda_global'] = "Pretendes realizar una búsqueda o exploración de algún lugar o servicio?"
       self.intent_mappings['saludar'] = "¿Esto es un saludo?"
       self.intent_mappings['insultar'] = "¿Me estás insultando?"
       self.intent_mappings['mas_info'] = "¿Quieres más información sobre alguno de los resultados anteriores?"
       self.intent_mappings['informar'] = "¿Me estás dando información que faltaba?"
       self.intent_mappings['info_chatbot_funciones'] = "¿Quieres decir que te gustaría saber que puedo hacer?"
       self.intent_mappings['afirmar'] = "¿Que estás de acuerdo?"
       self.intent_mappings['despedirse'] = "¿Te refieres a que te despides?" 
       self.intent_mappings['evaluacion_negativa'] = "¿Te refieres a que no he funcionado bien? :("
       self.intent_mappings['evaluacion_positiva'] = "¿Te refieres a que lo estoy haciendo bien?"
       self.intent_mappings['info_chatbot_construccion'] = "¿Te refieres a como estoy hecho?"
       self.intent_mappings['info_chatbot_creador'] = "¿Quieres saber quien me hizo?"
       self.intent_mappings['info_chatbot_identidad'] = "¿Te refieres a si soy humano o un robot?"
       self.intent_mappings['info_chatbot_nombre'] = "¿Me preguntas por mi nombre?"
       self.intent_mappings['negar'] = "¿Esto es una negativa?" 
       self.intent_mappings['nombre_usuario'] = "¿Este es tu nombre?"
       self.intent_mappings['querer_ejemplos'] = "¿Quieres que te de ejemplos para buscar?"
       self.intent_mappings['reirse'] = "¿Eso son risas?"
       self.intent_mappings['fuera_de_ambito'] = "¿Estas preguntando por alguna recomendacion ajeno a lugares?"


   def run(self, dispatcher, tracker, domain):

        # get the most likely intent
        last_intent_name = tracker.latest_message['intent']['name']
        print('tracker.latest_message = ' + str( tracker.latest_message))
        print('last_intent_name: ' + str(last_intent_name))
        intent_to_return = None
        message = None
       
        if last_intent_name != None:
            # get the prompt for the intent
            intent_ask = self.intent_mappings[last_intent_name]
       
            # Create the affirmation message and add two buttons to it.
            # Use '/<intent_name>' as payload to directly trigger '<intent_name>'
            # when the button is clicked.
            message = intent_ask

        elif tracker.get_slot('requested_slot') == "lugar":
            message = "¿Esto es un lugar donde buscar?"

        elif tracker.get_slot('requested_slot') == "servicio":
            message = "¿Esto es lo que quieres buscar?"

        if message:
            buttons = [{'title': 'Sí',

                       'payload': '/{}'.format(last_intent_name)},

                      {'title': 'No',

                       'payload': '/fuera_de_ambito'}]

            dispatcher.utter_button_message(message, buttons=buttons)
        else:            
            dispatcher.utter_template('utter_default', tracker)            

        return []

class ActionDefaultFallback(Action):

   """Asks for an affirmation of the intent if NLU threshold is not met."""

   def name(self):

       return "action_default_fallback"

   def run(self, dispatcher, tracker, domain):
       eventList = []

       tracker.activeForm = {}
       eventList.append(SlotSet("lugar", None))
       eventList.append(SlotSet("result", None))
       eventList.append(SlotSet("seleccion", None))
       eventList.append(SlotSet("servicio", None))

       dispatcher.utter_template('utter_default', tracker)

       return eventList

