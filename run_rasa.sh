#!/bin/bash


if [ "$1" == "a" ] # Actions Server
then
	kill -9 $(ps -ef | grep "rasa run actions" | awk 'FNR==1 {print $2}')
	sleep 2
	rasa run actions -v --debug  > rasa_actions.log 2>&1 &
elif [ "$1" == "s" ] # RASA Server
then
	kill -9 $(ps -ef | grep "rasa run -v" | awk 'FNR==1 {print $2}')
	sleep 2
	rasa run -v --debug --credentials credentials.yml --endpoints endpoints.yml  --enable-api &
elif [ "$1" == "" ] # Both Servers
then
	kill -9 $(ps -ef | grep "rasa run actions" | awk 'FNR==1 {print $2}')
	kill -9 $(ps -ef | grep "rasa run -v" | awk 'FNR==1 {print $2}')
	sleep 2
	rasa run -v --debug --credentials credentials.yml --endpoints endpoints.yml  --enable-api &
	rasa run actions -v --debug &
fi


sleep 1
ps -ef | grep "rasa run"

