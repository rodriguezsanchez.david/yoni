## intent:fuera_de_ambito
- dime el tiempo para mañana
- Dime el tiempo para el fin de semana
- puedes decirme el tiempo para mañana
- hay alguno abierto ahora
- me puedes decir los horarios
- conoces los horarios
- sabes a que hora cierra
- a que hora abre
- está abierto ahora
- el tiempo
- programación televisión
- progaramacion television
- cartelera
- cartelera cine
- poner alarma
- poner recordatorio

## intent:informar
- en [Madrid](lugar)
- En [Chiclana](lugar)

## intent:busqueda_global
- buscame un restaurante [chino](tipo_comida)
- buscame un restaurante [mejicano](tipo_comida)
- buscame un restaurante [italiano](tipo_comida)
- buscame un restaurante [vegetariano](tipo_comida)
- buscame un restaurante [vegano](tipo_comida)
- buscame un restaurante [comida casera](tipo_comida)
- buscame un restaurante [pescado](tipo_comida)
- nos gustaria comer en un [chino](tipo_comida)
- donde hay un buen [mejicano](tipo_comida)
- buscame en Chiclana un [italiano](tipo_comida)
- puedes localizarme un [chino](tipo_comida) en [Chiclana]
- Podrías encontrar  un sitio para cenar esta noche en Cádiz?
- que me digas un [restaurante](servicio)
- que me digas un [restaurante](servicio)
- pues dime algo de [hoteles](servicio)

## intent:despedida
- adios
- nos vemos
- hasta luego
- ha sido un placer
- hasta ahora
- bye
- adioss
- adiosss
- adiossss
- ciao


## intent:evaluacion_negativa
- mal
- fatal
- regular
- no muy bien
- pesimo
- porqueria
- no funciona

## intent:evaluacion_positiva
- afirmativo
- perfecto
- fantástico
- genial
- increible
- bien
- okay
- yes
- yess
- yesss
- yep
- bravo

## intent:info_chatbot_construccion
- como estas hecho?
- que tecnología usas?

## intent:info_chatbot_creador
- quien te hizo?
- quien te fabrico?
- quien te diseño?
- de quien eres?
- quien es tu creador?

## intent:info_chatbot_funciones
- que puedes hacer?
- que puedes ofrecerme?
- cuales son tus funciones?

## intent:info_chatbot_identidad
- quien eres?
- eres humano?
- eres una inteligencia artificial?
- eres un humano?
- eres un bot?
- eres un chatbot?
- eres un robot?

## intent:info_chatbot_nombre
- como te llamas?
- cual es tu nombre?
- tu nombre

## intent:negativo
- no

## intent:nombre_usuario
- soy [David](nombre)
- Soy [María](nombre)
- Me llamo [Roberto](nombre)
- me llamo [Juanjo](nombre)
- mi nombre es [Laura](nombre)

## intent:agradecer
- gracias
- Gracias
- muchas gracias
- Muchas gracias
- Muchas Gracias
- thanks
- Thanks
- thank you
- Thank you
- zenkiu
- Zenkiu

## synonym:restaurante
- comer
- cenar
- almorzar
- papear
- sitio donde comer
- sitio donde cenar
- sitio donde papear
- sitio donde almorzar
- sitio para comer
- sitio para cenar
- sitio para papear
- restaurante para cenar
- restaurante para comer
- restaurante para almorzar

## synonym:cafetería
- tomar café
- desayunar
- sitio donde desayunar
- sitio para desayunar
- merendar
- sitio donde merendar
- sitio para merendar
