## saludo basico
* saludar
  - utter_saludar

## nombre_usuario
* nombre_usuario{"nombre": "Ricardo"}
 - accion_establecer_nombre
   - slot{"nombre": "Ricardo"}
* nombre_usuario{"nombre": "María"}
 - accion_establecer_nombre
   - slot{"nombre": "María"}

## Informacion nombre Chatbot
* info_chatbot_nombre
  - utter_resp_info_chatbot_nombre

## Informacion chatbot funciones
* info_chatbot_funciones
  - utter_resp_info_chatbot_funciones
  - utter_ask_intento

## Simple afirmacion
* afirmar
  -utter_afirmar

## Simple negar
* negar  
  - utter_negar

## despedirsei v1
* despedirse
  - utter_despedirse
  - action_restart

## Informacion construccion chatbot
* info_chatbot_construccion  
  - utter_resp_info_chatbot_construccion

## Informacion chatbot creador
* info_chatbot_creador  
  - utter_resp_info_chatbot_creador

## Fuera de ambito+pcoord_usuario
* fuera_de_ambito
  - utter_fuera_de_ambito
  - utter_resp_info_chatbot_funciones
  - utter_ask_intento

## abandonar
* stop
  - utter_preg_abandonar
* afirmar
  - utter_afirmar
  - action_restart  

## abandonar
* stop
  - utter_preg_abandonar
* negar
  - utter_negar


## Risas
* reirse
  - utter_reirse

## Insultar
* insultar
  - utter_insultar

## Insultar plus
* insultar
  - utter_insultar
* insultar
  - utter_insultar
  - utter_preg_abandonar
* afirmar
  - utter_afirmar
  - action_restart

## Insultar plus
* insultar
  - utter_insultar
* insultar
  - utter_insultar
  - utter_preg_abandonar
* negar
  - utter_afirmar

##Evaluacion Positiva
* evaluacion_positiva
  - utter_resp_eval_positiva

##Evaluacion Negativa
* evaluacion_negativa
  - utter_resp_eval_negativa
  - action_restart

<!-- *********************************** -->
<!-- Historias de busqueda y exploracion -->
<!-- *********************************** -->


## Story busqueda_global 1
* busqueda_global{"servicio":"casa de alquiler","lugar":"Galicia"}
    - busqueda_form
    - form{"name": "busqueda_form"}
    - slot{"lugar":"Galicia"}
    - slot{"servicio":"casa de alquiler"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - accion_mostrar_resultado
    - utter_preg_evaluacion


## Story busqueda_global 2
* busqueda_global{"servicio":"museo"}
    - busqueda_form
    - form{"name": "busqueda_form"}
    - slot{"servicio":"museo"}
    - slot{"requested_slot":"lugar"}
* form: inform{"lugar":"Cádiz"}
    - slot{"lugar":"Cádiz"}
    - form: busqueda_form
    - slot{"lugar":"Cádiz"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - accion_mostrar_resultado
    - utter_preg_evaluacion

## Story busqueda_global 2
* busqueda_global{"servicio":"gimnasio"}
    - busqueda_form
    - form{"name": "busqueda_form"}
    - slot{"servicio":"gimnasio"}
    - slot{"requested_slot":"lugar"}
* form: inform{"lugar":"Chiclana"}
    - slot{"lugar":"Chiclana"}
    - form: busqueda_form
    - slot{"lugar":"Chiclana"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - accion_mostrar_resultado
    - utter_preg_evaluacion

## Story busqueda_global 3
* busqueda_global{"lugar":"La Rioja"}
    - busqueda_form
    - form{"name": "busqueda_form"}
    - slot{"lugar":"La Rioja"}
    - slot{"requested_slot":"servicio"}
* form: inform{"servicio":"cafetería"}
    - slot{"servicio":"cafetería"}
    - form: busqueda_form
    - slot{"servicio":"cafetería"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - accion_mostrar_resultado
    - utter_preg_evaluacion

## Story busqueda_global 3
* busqueda_global{"lugar":"Sevilla"}
    - busqueda_form
    - form{"name": "busqueda_form"}
    - slot{"lugar":"Sevilla"}
    - slot{"requested_slot":"servicio"}
* form: inform{"servicio":"skatepark"}
    - slot{"servicio":"skatepark"}
    - form: busqueda_form
    - slot{"servicio":"skatepark"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - accion_mostrar_resultado
    - utter_preg_evaluacion

## Story busqueda_global 4
* busqueda_global{"servicio":"gasolinera","lugar":"Jerez"}
    - busqueda_form
    - form{"name": "busqueda_form"}
    - slot{"lugar":"Jerez"}
    - slot{"servicio":"gasolinera"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - accion_mostrar_resultado
    - utter_preg_evaluacion


## Generated Story 6286235565496346919
* fuera_de_ambito
    - utter_fuera_de_ambito


## Generated Story -8122602981954943179
* fuera_de_ambito{"servicio": "el tiempo"}
    - utter_fuera_de_ambito


## Generated Story -7199855341781317107
* querer_ejemplos
    - utter_dar_ejemplos

## McDonald en Chiclana
* busqueda_global{"servicio": "McDonald", "lugar": "Chiclana"}
    - busqueda_form
    - form{"name": "busqueda_form"}
    - slot{"lugar": "Chiclana"}
    - slot{"servicio": "McDonald"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - accion_mostrar_resultado
    - utter_preg_evaluacion


## Generated Story 5915077265431809166
* info_chatbot_funciones
    - utter_resp_info_chatbot_funciones


## Generated Story 4293291181248991013
* fuera_de_ambito{"seleccion": "vuelos"}
    - utter_fuera_de_ambito

## Flujo de Agradecimiento
* agradecer
    - utter_agradecer
