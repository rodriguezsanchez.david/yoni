''' Parser para extraer un csv de lo datos de conversaciones de la BD para calcular métricas '''

import conversations_fromDB_2 as conversations #Debe cambiarse para cada iteración

OUTPUT_FILENAME="conversations_data_2.csv"# El nombre de fichero de salida con los datos parseados de cada iteración

parsed_line = None
with open(OUTPUT_FILENAME, 'w') as out_file:
    header = "event\tname\ttext\tentities\n"
    out_file.write(header)
    for s in conversations.sentences:
        if s["event"] == "user":
            parsed_line = "intent" + '\t' + str(s['parse_data']['intent']['name']) + '\t' + str(s['text']) + '\t' +  str(s['parse_data']['entities']) + '\n' # intent
        elif s["event"] == "action":
            parsed_line = "next_action" + '\t' + str(s['name']) + '\n' #action
        elif s["event"] == "slot":
            parsed_line = "entity" + '\t' + str(s['name']) +  '\t' + str(s['value']) + '\n' #entity

        if parsed_line:
            out_file.write(parsed_line)
        parsed_line = None

